#include <iostream>
#include <filesystem>
#include <list>
#include <string>

int main()
{
    std::string our_path = "";
    std::string our_extension = "";
    std::cout << "Enter the file-system path with double \\-s !!!" << std::endl;
    std::getline(std::cin, our_path);
    //////
    if( !std::filesystem::exists(our_path) )
    {
        std::cout << "Incorrect path !!!" << std::endl;
        return -1;
    }
    ///////
    std::cout << "Enter the file extension with point !!!" << std::endl;
    std::cin >> our_extension;

    auto recursiveGetFileNamesByExtension = [](const std::filesystem::path &path_, const std::string &extension_)
    {
        std::list <std::string> files_lst;

        for( auto& pths: std::filesystem::recursive_directory_iterator(path_) )
        {
            if(pths.is_regular_file())
            {
                if( ! pths.path().extension().compare(extension_) ) files_lst.push_back(pths.path().filename());
            }
        }

        return files_lst;
    }(our_path, our_extension);

    for( auto & list_member: recursiveGetFileNamesByExtension)
    {
        std::cout << list_member << " " << std::endl;
    }

    return 0;
}

